define([
    'base/js/namespace',
    'jquery',
    'base/js/utils',
    'base/js/dialog',
    './publish'
], function(Jupyter, $, utils, dialog, button){
    function load_ipython_extension(){
        button.create_button();
    }
    
    return {
        load_ipython_extension: load_ipython_extension
    };
});
