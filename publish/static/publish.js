define([
    'base/js/namespace',
    'jquery',
    'base/js/utils',
    'base/js/i18n',
    'base/js/dialog',
    'notebook/js/keyboardmanager',
    'base/js/events'
    
], function(Jupyter, $, utils, i18n, dialog, keyboardmanager, events){
    // var us = requirejs('static/services/contents');
    var keyboard_manager = new keyboardmanager.KeyboardManager({events: events});
    function create_button(){
        if (!Jupyter.toolbar) {
            $([Jupyter.events]).on("app_initialized.NotebookApp", create_button);
            return;
        }
        Jupyter.toolbar.add_buttons_group([{
            label: 'Publish  ',
            icon: ' ',
            callback: save_notebook,
            id: 'my_save_notebook_as'
        }]);
        
    }
    
    
    function save_notebook(){ 
        // console.log(Jupyter.notebook.base_url);
        // Jupyter.notebook.save_notebook_as(); 
        var that = Jupyter.notebook; // this is the current notebook
        var current_dir = $('body').attr('data-notebook-path').split('/').slice(0, -1).join("/");
        console.log(current_dir);
        current_dir = current_dir? current_dir + "/": "";
        var dialog_body = $('<div/>').append(
            $('<p/>').addClass('my-save-message')
                .text(i18n.msg._('Enter a notebook path relative to configured save dir'))
        ).append(
            $('<br/>')
        ).append(
            $('<input/>').attr('type','text').attr('size','25')
            // .attr('data-testid', 'save-as')
            .addClass('form-control')
        );

        var d = dialog.modal({
            title: 'Save As',
            body: dialog_body,
            keyboard_manager: keyboard_manager,
            notebook: Jupyter.notebook, //or you can say 'that'
            buttons: {
                Cancel: {},
                Save: {
                    class: 'btn-primary',
                    click: function() {
                        var nb_path = d.find('input').val();
                        var nb_name = nb_path.split('/').slice(-1).pop();
                        // If notebook name does not contain extension '.ipynb' add it
                        var ext = utils.splitext(nb_name)[1];
                        if (ext === '') {
                            nb_name = nb_name + '.ipynb';
                            nb_path = nb_path + '.ipynb';
                        }
                        var save_thunk = function() {
                            var model = {
                                'type': 'notebook_publish',
                                'content': that.toJSON(),
                                'name': nb_name,
                                'publish': 'yes'
                            };
                            //change this to ajax call? change content manager class hybrid
                            return that.contents.save(nb_path, model) // change this to function from publish? or smth
                                .then(function(data) {
                                    d.modal('hide');
                                    // that.notebook_name = data.name;
                                    // that.notebook_path = data.path;
                                    // that.session.rename_notebook(data.path);
                                    // that.events.trigger('notebook_renamed.Notebook', data);
                                }, function(error) {
                                    var msg = i18n.msg._(error.message || 'Unknown error saving notebook');
                                    $(".my-save-message").html(
                                        $("<span>")
                                            .attr("style", "color:red;")
                                            .text(msg)
                                    );
                                });
                        };
                        that.contents.get(nb_path, {type: 'notebook_publish', content: false}).then(function(data) {
                            console.log("get din frontend overwrite");
                            var warning_body = $('<div/>').append(
                                $("<p/>").text(i18n.msg._('Notebook with that name exists.')));
                            dialog.modal({
                                title: 'Save As',
                                body: warning_body,
                                buttons: {Cancel: {},
                                Overwrite: {
                                    class: 'btn-warning',
                                    click: function() {
                                        return save_thunk();
                                    }
                                }
                            }
                            });
                        }, function(err) {
                            return save_thunk();
                        });
                        return false;
                    }
                },
            },
            open : function () {
                d.find('input[type="text"]').keydown(function (event) {
                    if (event.which === keyboard.keycodes.enter) {
                        d.find('.btn-primary').first().click();
                        return false;
                    }
                });
                d.find('input[type="text"]').val(current_dir).focus();
             }
         });
    };
    
    return {
        create_button: create_button
    };
});
