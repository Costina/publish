from setuptools import setup, find_packages


setup(name='publish',
      description='A button in the Jupyter Notebook frontend',
      include_package_data=True,
      url="https://github.com/CostinaB/publish",
      packages=find_packages(),
      zip_safe=False,
      install_requires=[
          'msgpack',
          'jupyter==1'
      ],
)